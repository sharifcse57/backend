# wedevs-project

> A Laravel API project

## Build Setup

```bash
# install dependencies
npm install
composer install

Edit env file and connect database
php artisan key:generate

php artisan migrate
php artisan passport:install

# build for dev with minification
npm run dev

# build for production with minification
npm run prod

# serve with at localhost:8000
php artisan serve


```

## Api Docs Routes URL with Verb

'headers' => [

    'Accept' => 'application/json',

    'Authorization' => 'Bearer '.$accessToken,

]

Login: Verb:GET, URL:http://localhost:8000/oauth/token

Register: Verb:GET, URL:http://localhost:8000/api/register

List: Verb:GET, URL:http://localhost:8000/api/products

Create: Verb:POST, URL:http://localhost:8000/api/products

Show: Verb:GET, URL:http://localhost:8000/api/products/{id}

Update: Verb:PUT, URL:http://localhost:8000/api/products/{id}

Delete: Verb:DELETE, URL:http://localhost:8000/api/products/{id}
